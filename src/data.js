export const dataSource = [
    {
        notf: 'HSBC Insurance Asia Equity Fund',
        fc: 'KMF1',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Focused Income Fund',
        fc: 'AFIF',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Asian Bond Fund',
        fc: 'ASBS',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance China Equity Fund',
        fc: 'CGFS',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Chinese Equity Fund',
        fc: 'CEFF',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Emerging Markets Equity Fund',
        fc: 'EMFS',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Ethical Global Equity Fund',
        fc: 'TMF1',
        cs: true,
        co: true,
        csa: false
    },
    {
        notf: 'HSBC Ethical Global Sukuk Fund',
        fc: 'TSFS',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Europe Dynamic Equity Fund',
        fc: 'EEFF',
        cs: true,
        co: false,
        csa: false
    },
    {
        notf: 'HSBC Insurance Global Bond Fund',
        fc: 'GBOS',
        cs: true,
        co: true,
        csa: true
    }
];