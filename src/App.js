import React, {Fragment, useState} from "react";
import {dataSource} from "./data";
import './App.css';
import {Button, Checkbox, Input, Select, Space, Table} from "antd";
import {unstable_batchedUpdates} from "react-dom";

function App() {
    const [rowCount, setRowCount] = useState(1);
    const [rows, setRows] = useState([{key: '1'}])

    const { Option } = Select;
    return (
        <Fragment>
            <Table
                pagination={false}
                dataSource={rows}
                columns={
                    [
                        {
                            title: 'Name of the Fund',
                            dataIndex: 'notf',
                            key: 'notf',
                            render: (text, record) => <Select
                                onSelect={(value => {
                                    const data = JSON.parse(value);
                                    const modified = rows?.filter(o=>o?.key!==data?.key)
                                    modified.push(data);
                                    const sorted = modified.sort((a,b) => parseInt(a?.key)-parseInt(b?.key))
                                    setRows(sorted)
                                })}
                                style={{minWidth: 200}}
                            >
                                {dataSource.map(o => <Option
                                    value={JSON.stringify({...o, ...record})}>{o?.notf}</Option>)}
                            </Select>
                        },
                        {
                            title: 'Fund Code',
                            dataIndex: 'fc',
                            key: 'fc'
                        },
                        {
                            title: 'Cash/SRS',
                            dataIndex: 'cs',
                            key: 'cs',
                            render: (text, record) => <Checkbox disabled checked={record?.cs}/>
                        },
                        {
                            title: 'CPFIS OA',
                            dataIndex: 'co',
                            key: 'co',
                            render: (text, record) => <Checkbox disabled checked={record?.co}/>
                        },
                        {
                            title: 'CPFIS SA',
                            dataIndex: 'csa',
                            key: 'csa',
                            render: (text, record) => <Checkbox disabled checked={record?.csa}/>
                        },
                        {
                            title: 'Switch Out',
                            dataIndex: 'so',
                            key: 'so',
                            render: () => <Input/>
                        },
                        {
                            title: 'Switch In',
                            dataIndex: 'si',
                            key: 'si',
                            render: () => <Input/>
                        }
                    ]
                }
            />
            <Space style={{float: "right"}}>
                <Button
                    style={{margin: 10}}
                    type="primary"
                    onClick={() => {
                        unstable_batchedUpdates(() => {
                            setRows([...rows, {key: (rowCount + 1).toString()}])
                            setRowCount(rowCount + 1)
                        })
                        setRowCount(rowCount + 1);
                    }}
                >Add</Button>
                <Button
                    style={{margin: 10}}
                    type="primary"
                    disabled={rowCount === 1}
                    onClick={() => {
                        unstable_batchedUpdates(() => {
                            const modified = rows.slice(0, rowCount - 1)
                            setRows(modified)
                            rowCount > 1 && setRowCount(rowCount - 1)
                        })
                    }}
                >Remove</Button>
            </Space>
        </Fragment>
    );
}

export default App;
